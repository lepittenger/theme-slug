var paths = {
    sass: './assets/src/sass/**/*.scss',
    css: './assets/dist/stylesheets/'
};
module.exports = paths; // makes paths accessible to other files
