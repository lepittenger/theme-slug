'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var compass = require('gulp-compass');
var paths = require('./paths');

gulp.task('styles', function() {
    return sass(paths.sass)
    .on('error', sass.logError)
    .pipe(gulp.dest(paths.css));
});

gulp.task("concatScripts", function(){
    gulp.src([
        'js/navigation.js',
        'js/skip-link-focus-fix.js'])
});

gulp.task('default', ['styles']);
